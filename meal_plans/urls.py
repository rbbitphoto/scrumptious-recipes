from django.urls import path

from meal_plans.views import (
    PlanListView,
    PlanCreateView,
    PlanDeleteView,
    PlanDetailView,
    PlanUpdateView,
)

urlpatterns = [
    path("", PlanListView.as_view(), name="meal_plans_list"),
    path(
        "<int:pk>/",
        PlanDetailView.as_view(),
        name="meal_plans_detail",
    ),
    path(
        "<int:pk>/edit",
        PlanUpdateView.as_view(),
        name="meal_plans_edit",
    ),
    path(
        "<int:pk>/delete/",
        PlanDeleteView.as_view(),
        name="meal_plans_delete",
    ),
    path("create/", PlanCreateView.as_view(), name="meal_plans_new"),
]
