from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


# Create your models here.
class MealPlan(models.Model):
    name = models.CharField(max_length=125)
    date = models.DateField(null=True)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="mealplans",
        on_delete=models.CASCADE,
        null=True,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipe")

    def __str__(self):
        return self.name + " by " + str(self.owner)
