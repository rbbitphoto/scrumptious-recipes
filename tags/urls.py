from django.urls import path

from tags.views import (
    TagCreateView,
    TagDeleteView,
    TagDetailView,
    TagListView,
    TagUpdateView
)

urlpatterns = [
    path("", TagListView.as_view(), name="tag_list"),
    path("<int:pk>/delete/", TagDeleteView.as_view(), name="tag_delete"),
    path("new/", TagCreateView.as_view(), name="tag_new"),
    path("<int:pk>/edit/", TagUpdateView.as_view(), name="tag_edit"),
    path("<int:pk>/", TagDetailView.as_view(), name="tag_detail"),
]
